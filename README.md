Just a quick python script to sort through a bunch of random files and sort them into alphabetical folders.
It also does md5sum on all files to remove any duplicates with different names.
If a file with the same name but different content is found it will be skipped. (Maybe I fix this later, it was not a priority for what I needed this for)
For my need, I also added that it will convert any characters in the name that is not Alphanumerical, a dsah (-) or an underscore (_) to an underscore.

Takes 2 arguments, source directory and destination directory

Destination needs to be empty.
