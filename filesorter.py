import argparse
import re
import hashlib
from pathlib import Path


# Print some status text on the same line
def same_line_print(the_text):
    print(the_text, end='\r')
    print(end='\x1b[2K')


# Read all files to be processed into a list.
def read_entries(source_dir):
    print(f"Reading files from {str(source_dir)}")
    files_to_process = []
    for entry in source_dir.rglob("*"):
        if entry.is_file():
            same_line_print(f"Adding {str(entry)}")
            files_to_process.append(entry)
    print(f"Found {len(files_to_process)} files to process.")
    return files_to_process


# Calculate md5 Hashes for files
def get_hashes(files):
    print("Generating md5 hashes")
    hashed_files = {}
    for file in files:
        same_line_print(f"Creating md5 hash for {str(file)}")
        hash_value = hashlib.md5()
        with open(file, "rb") as f:
            for byte_block in iter(lambda: f.read(4096), b""):
                hash_value.update(byte_block)
            hashed_files[file] = hash_value.hexdigest()
    return hashed_files


# Find duplicates
def check_duplicates(hashed_files):
    print("Looking for duplicates")
    hash_duplicates_removed = {}
    duplicate_hashes = []
    for file, hash_value in hashed_files.items():
        if hash_value not in hash_duplicates_removed.values():
            hash_duplicates_removed[file] = hash_value
        else:
            duplicate_hashes.append(hash_value)
    duplicate_hashes = list(set(duplicate_hashes))
    print(f"Found a total of {len(duplicate_hashes)} duplicate hashes.")
    valid_input = ["y", "n"]
    the_input = ""
    while the_input not in valid_input:
        the_input = input("Do you want to auto-select the first duplicate file to copy?\n"
                          "If you select (N)o, you will have to manually select each file. Y/N: ").lower()
    for hashes in duplicate_hashes:
        duplicate_values = [k for k, v in hashed_files.items() if v == hashes]
        text_prompt = ""
        if the_input == "n":
            text_prompt = f"{len(duplicate_values)} duplicate files found with hash {hashes}"
            text_lines = len(text_prompt) * "-"
            text_prompt = f"\n{text_lines}\n{text_prompt}\n{text_lines}\n\n"

        list_files = {}
        for i in range(0, len(duplicate_values)):
            if the_input == "n":
                text_prompt += f"{i+1} - {str(duplicate_values[i])}\n"
            list_files[str(i+1)] = duplicate_values[i]
        if the_input == "n":
            print(f"{text_prompt}\n")
            keep_file = ""
        else:
            keep_file = "1"
        while keep_file not in list_files.keys():
            keep_file = input("Which file should be copied? ")
        for file_nr, file in list_files.items():
            if file_nr != keep_file:
                del hashed_files[file]
    return hashed_files


# Generate destination letters based on first letter of filenames.
def get_directory_letters(files):
    directory_letters = []
    for file in files:
        first_letter = file.name[0].upper()
        if not first_letter.isalpha():
            first_letter = "#"
        directory_letters.append(first_letter)
    directory_letters = list(set(directory_letters))
    directory_letters.sort()
    return directory_letters


# Create the destination directories to put the files in
def create_directory_letters(dest_dir, directory_letters):
    print(f"Creating directories at {str(dest_dir)} to sort files into.")
    for letter in directory_letters:
        new_dir = dest_dir / letter
        new_dir.mkdir(parents=False, exist_ok=False)


# Copy the files to destination directory
def copy_files(dest_dir, files):
    print(f"Copying {len(files)} files.")
    for file in files:
        same_line_print(f"Copying {str(file)}")
        first_letter = file.name[0].upper()
        if not first_letter.isalpha():
            first_letter = "#"
        file_name = f"{cleanup_filename(file.stem)}{file.suffix}"
        destination = dest_dir.joinpath(first_letter, file_name)
        same_line_print(f"Copying to {str(destination)}")
        if not destination.exists():
            with destination.open(mode='xb') as fid:
                fid.write(file.read_bytes())


# Cleanup the filename
def cleanup_filename(file_name):
    return re.sub(r"[^A-Za-z/d_\-]", "_", file_name)


# The main process
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    parser.add_argument("dest")
    args = parser.parse_args()

    source_dir = Path(args.path)
    dest_dir = Path(args.dest)

    if not source_dir.exists():
        print("The source directory doesn't exist")
        raise SystemExit(1)

    if not dest_dir.exists():
        print("The destination directory doesn't exist")
        raise SystemExit(1)

    if any(Path(dest_dir).iterdir()):
        print("Can only send files to an empty directory.")
        raise SystemExit(1)

    my_files = read_entries(source_dir)
    hashed_files = get_hashes(my_files)
    filelist = check_duplicates(hashed_files)
    directory_letters = get_directory_letters(my_files)
    create_directory_letters(dest_dir, directory_letters)
    copy_files(dest_dir, filelist)


if __name__ == "__main__":
    main()